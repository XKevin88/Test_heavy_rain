<?php

require 'CityBuilder.php';

/**
 *
 */
class HeavyRain extends CityBuilder
{
    function __construct(){}

    public function exec($rand = false)
    {
        $city   = $rand ? $this->randomCity() : $this->staticCity();
        $result = 0;

        /* add your code here */

        $size = count($city);
      //  echo $size."\n".$max."\n";

        /* i_Min est l'indice de départ */
        $i_Min = 0;
        for ($i_Min=0; $i_Min<$size-1; $i_Min++){
        	if ($city[$i_Min]>$city[$i_Min+1]) break;
        }

        for ($i=$i_Min; $i<$size-1; $i++){

        	$heightMax = 0;
        	$indexMax = $i;
        	for ($j=$i+1; $j<$size; $j++){
        		if ($city[$i] <= $city[$j]){
        			$heightMax = $city[$j];
        			$indexMax = $j;
        			break;
        		}
        	}
        	
        	/* Si on a pas de trouvé d'immeuble plus haut, alors $city[$i] est déjà le plus haut */
        	if ($heightMax == 0){
        		for ($j=$i+1; $j<$size; $j++){
	        		if ($heightMax < $city[$j]){
	        			$heightMax = $city[$j];
	        			$indexMax = $j;
	        		}
	        	}

	        	/* Si l'immeuble le plus haut est celui en $i+1, on passe cette itération */
	        	if ($indexMax == $i){
	        		break;
	        	} 
        	}

        	$tailleBloc = 0;

        	/* Le minimum des deux extrémités */
        	$minHeight = $city[$i] < $heightMax ? $city[$i] : $heightMax;

        	/* Calcul de la taille du bloc d'eau actuel */
        	for ($j=$i+1; $j<$indexMax; $j++){
        		$tailleBloc += $minHeight - $city[$j];
        	}

        	//echo "taille bloc = ".$tailleBloc."\n";

        	$result = $tailleBloc > $result ? $tailleBloc : $result;

        	$i = $indexMax-1;
        }

        echo json_encode($city) . " => " . $result . "\n";
    }
}

$HV = new HeavyRain();

$HV->exec(false);
